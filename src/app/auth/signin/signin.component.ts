import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from "../../../shared/services/auth";
import { LocalStorageUtils } from "../../../shared/utils/localStorage";


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(
    private router: Router, 
    private authService: AuthService,
    private localStorageUtils: LocalStorageUtils) { }
  credentials: any = {
    password: "",
    email: ""
  };

    // When true, all actions are disabled (used while saving form).
    actionsDisabled: any = false;

  ngOnInit() {

  }

  onSubmit() {
    // Get available subjects from the server.
    this.authService.signinUser(this.credentials.email, this.credentials.password).then(
      (data) => { 
        // Successfull login. Simulate token storing to localstorage and go to homepage.
        let token = data;
        this.localStorageUtils.setToken(JSON.stringify(token));
        this.router.navigate(["/"]);
      },
      (err) => {
        console.log(err);
        if (err === "Invalid credentials") {
          alert("Email or password is incorrect !");
        }
        // TODO: Handle error/redirect to error page.
      }
    );
  }
}
