import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {StudentService} from "../../shared/services/student";
import {SubjectService} from "../../shared/services/subject";


@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {
  subjects: any;
  studentId: any;
  mode: any;
  student: any = {
    id: new Date().valueOf().toString(), // TODO: Implement actual unique id.
    firstName: "",
    lastName: "",
    email: "",
    subjects: []
  };

  // Flags
  // Needed to display loader/form.
  isPageLoaded: any = false;
  // When true, all actions are disabled (used while saving form).
  actionsDisabled: any = false;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private studentService: StudentService,
    private subjectService: SubjectService
    ) { }

  ngOnInit() {
    // Needed to display in new or edit mode.
    this.mode = this.route.snapshot.url[0].path;

    // TODO: "edit" mode should be made an enum in enums file.
    if (this.mode === "edit") {
      // Student and subjects list are needed.
      this.studentId = this.route.snapshot.params["id"];

      // Get student from the server.
      this.studentService.getStudent(this.studentId).then(
        (data) => { 
          this.student = data;
          this.getSubjects();
        },
        (err) => {
          console.log(err);
          if (err === "Token mismatch") {
            // Authentication expired/error - go to login page.
           this.router.navigate(["/login"]);
          }
          // TODO: Handle error/redirect to error page.
        }
      );
    } else {
      // Mode is new - only subjects list is needed.
      this.getSubjects();
    }
  }

  getSubjects() {
    // Get available subjects from the server.
    this.subjectService.getSubjects().then(
      (data) => { 
        this.subjects = data;
        // Needed data was received - display form.
        this.isPageLoaded = true;
      },
      (err) => {
        console.log(err);
        if (err === "Token mismatch") {
          // Authentication expired/error - go to login page.
         this.router.navigate(["/login"]);
        }
        // TODO: Handle error/redirect to error page.
      }
    );
  }

  onSubmit() {
    if (this.mode === "edit") {
      this.updateStudent();
    } else {
      this.createStudent();
    }
  }

  updateStudent() {
    // Disable actions while user is being updated;
    this.actionsDisabled = true;

    // Update student and navigato to overview.
    this.studentService.updateStudent(this.student).then(
      (data) => { 
        this.actionsDisabled = false;
        this.router.navigate(["/"]);
      },
      (err) => {
        console.log(err);
        if (err === "Token mismatch") {
          // Authentication expired/error - go to login page.
         this.router.navigate(["/login"]);
        }
        // TODO: Handle error/redirect to error page.
      }
    );
  }

  createStudent() {
    // Disable actions while user is being created;
    this.actionsDisabled = true;

    // Create student and navigato to overview.
    this.studentService.createStudent(this.student).then(
      (data) => { 
        this.actionsDisabled = false;
        this.router.navigate(["/"]);
      },
      (err) => {
        console.log(err);
        if (err === "Token mismatch") {
          // Authentication expired/error - go to login page.
         this.router.navigate(["/login"]);
        }
        // TODO: Handle error/redirect to error page.
      }
    );
  }

  onExitForm() {
    this.router.navigate(["/"]);
  }
}

