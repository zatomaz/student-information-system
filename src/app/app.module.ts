import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OverviewComponent } from './overview/overview.component';
import { StudentFormComponent } from './student-form/student-form.component';
import { SigninComponent } from './auth/signin/signin.component';
import {StudentService} from "../shared/services/student";
import {SubjectService} from "../shared/services/subject";
import { AuthService } from "../shared/services/auth";
import { LocalStorageUtils } from "../shared/utils/localStorage";

const appRoutes: Routes = [
  { path: '' , component: OverviewComponent }, // OverviewComponent should be the homepage.
  { path: 'edit/:id' , component: StudentFormComponent },
  { path: 'new' , component: StudentFormComponent },
  { path: 'login' , component: SigninComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    OverviewComponent,
    StudentFormComponent,
    SigninComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    MultiSelectModule,
    BrowserAnimationsModule
  ],
  providers: [
    StudentService,
    SubjectService,
    AuthService,
    LocalStorageUtils
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
