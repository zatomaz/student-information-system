import { Component, OnInit } from '@angular/core';
import {StudentService} from "../../shared/services/student";
import { Router } from '@angular/router';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {
  students: any = [];
  pager: any = {};
  pagedStudents: any = [];
  pageSize: number = 20;

  // Flags.
  // Needed to display loader and to enable actions.
  isPageLoaded: any = false;
  // When true, all actions (add, edit, delete students) are disabled.
  actionsDisabled: any = false;

  constructor(private router: Router, private studentService: StudentService) { }

  ngOnInit() {
    // Load grid on page 1.
    this.loadGrid(1);
  }

  loadGrid(page: number) {
    this.isPageLoaded = false;

    this.studentService.getStudents().then(
     (data) => { 
       this.students = data;
       this.setPage(page);
       // Page is loaded - hide spinner and display data.
       this.isPageLoaded = true;
     },
     (err) => {
       this.isPageLoaded = false;
       console.log(err);
       if (err === "Token mismatch") {
         // Authentication expired/error - go to login page.
        this.router.navigate(["/login"]);
       }
       // TODO: Handle error/redirect to error page.
     }
   );
  }

  setPage(page: number) {
    // Get pager object.
    this.pager = this.getPager(this.students.length, page);

    // Get current page of students.
    this.pagedStudents = this.students.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  getPager(totalItems: number, currentPage: number = 1) {
    // Calculate total pages.
    let totalPages = Math.ceil(totalItems / this.pageSize);

    // Ensure current page isn't out of range.
    if (currentPage < 1) { 
        currentPage = 1; 
    } else if (currentPage > totalPages) { 
        currentPage = totalPages; 
    }
    
    let startPage: number, endPage: number;
    if (totalPages <= 10) {
        // Less than 10 total pages so show all.
        startPage = 1;
        endPage = totalPages;
    } else {
        // More than 10 total pages so calculate start and end pages.
        if (currentPage <= 6) {
            startPage = 1;
            endPage = 10;
        } else if (currentPage + 4 >= totalPages) {
            startPage = totalPages - 9;
            endPage = totalPages;
        } else {
            startPage = currentPage - 5;
            endPage = currentPage + 4;
        }
    }

    // Calculate start and end item indexes.
    let startIndex = (currentPage - 1) * this.pageSize;
    let endIndex = Math.min(startIndex + this.pageSize - 1, totalItems - 1);

    // Create an array of pages to ng-repeat in the pager control.
    let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

    // Return object with all pager properties required by the view.
    return {
        totalItems: totalItems,
        currentPage: currentPage,
        totalPages: totalPages,
        startPage: startPage,
        endPage: endPage,
        startIndex: startIndex,
        endIndex: endIndex,
        pages: pages
    };
}

  onCreateStudent() {
    // Handle action.
    this.router.navigate(["/new"]);
  }

  onEditStudent(student: any) {
    // Handle action.
    this.router.navigate(["/edit/" + student.id]);
  }

  onDeleteStudent(index: number) {
    // Calculate student index in students array.
    let studentIndex: number = (this.pager.currentPage - 1) * this.pageSize + index;
    // Disable actions while user is being deleted;
    this.actionsDisabled = true;
    // Delete student and reload grid.
    this.studentService.deleteStudent(studentIndex).then(
      (data) => { 
        if (this.pager.currentPage === 1) {
          // Stay on page 1.
          this.loadGrid(1);
        } else if (this.pagedStudents.length === 1) {
          // Last student on the page was deleted - go to previous page.
          this.loadGrid(this.pager.currentPage - 1);
        } else {
          // Current page contains more students - stay on this page.
          this.loadGrid(this.pager.currentPage);
        }

        
        this.actionsDisabled = false;
      },
      (err) => {
        console.log(err);
        if (err === "Token mismatch") {
          // Authentication expired/error - go to login page.
         this.router.navigate(["/login"]);
        }
        // TODO: Handle error/redirect to error page.
      }
    );
  }
}
