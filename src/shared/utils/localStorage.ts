import {Injectable} from "@angular/core";

@Injectable()
export class LocalStorageUtils {
  getToken() {
    return JSON.parse(localStorage.getItem("auth-token"));
  }

  setToken(token: string) {
    localStorage.setItem("auth-token", token); 
  }
}