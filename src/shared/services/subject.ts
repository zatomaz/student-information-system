import {Injectable} from "@angular/core";
import { LocalStorageUtils } from "../../shared/utils/localStorage";

@Injectable()
export class SubjectService {
  constructor(private localStorageUtils: LocalStorageUtils) { }

  // Hardcoded subjects data. 
  serverToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dnZWRJbkFzIjoiYWRtaW4iLCJpYXQiOjE0MjI3Nzk2Mzh9.gzSraSYS8EXBxLN_oWnFSRgCzcmJmMjLiuyu5CSpyHI";
  subjects: any =[
    {label: 'Diskretne strukture', value: 'Diskretne strukture'},
    {label: 'Programianje 2', value: 'Programianje 2'},
    {label: 'Multimedija', value: 'Multimedija'},
    {label: 'Algebra', value: 'Algebra'},
    {label: 'Programianje 3', value: 'Programianje 3'},
    {label: 'Umetna inteligenca', value: 'Umetna inteligenca'}
  ]

  getSubjects() {
    return new Promise((resolve, reject) => {
    // Simulate success/error.
    let error: any = false;
    // Get token.
    let clientToken = this.localStorageUtils.getToken();
    
    setTimeout(() => {
          if (this.serverToken !== clientToken) {
            reject("Token mismatch"); // Authentication error.
          } if (error) {
              reject("error"); // Simulates error.
          } else {
            resolve(this.subjects); // Simulates success.
          }
        }, 300);
    });
  }
}