import {Injectable} from "@angular/core";
import { LocalStorageUtils } from "../../shared/utils/localStorage";

@Injectable()
export class StudentService {
  constructor(private localStorageUtils: LocalStorageUtils) { }

    // Hardcoded initial students data. 
    serverToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dnZWRJbkFzIjoiYWRtaW4iLCJpYXQiOjE0MjI3Nzk2Mzh9.gzSraSYS8EXBxLN_oWnFSRgCzcmJmMjLiuyu5CSpyHI";
    students: any = [
      {
        id:"00000000000001", 
        firstName: "Anja",
        lastName: "Jenko",
        email: "anja.jenko@gmail.com",
        subjects: ["Diskretne strukture", "Multimedija"]
      },
      {
        id:"00000000000002", 
        firstName: "Miha",
        lastName: "Marinko",
        email: "miha.marinko@gmail.com",
        subjects: ["Diskretne strukture", "Programianje 3"]
      },
      {
        id:"00000000000003", 
        firstName: "Janez",
        lastName: "Novak",
        email: "janez.novak@gmail.com",
        subjects: ["Programianje 2", "Multimedija"]
      },
      {
        id:"00000000000004", 
        firstName: "Aljaž",
        lastName: "Rode",
        email: "aljaz.rode@gmail.com",
        subjects: ["Diskretne strukture"]
      },
      {
        id:"00000000000005", 
        firstName: "Matej",
        lastName: "Arko",
        subjects: ["Diskretne strukture", "Multimedija"]
      },
      {
        id:"00000000000006", 
        firstName: "Ines",
        lastName: "Jek",
        subjects: ["Programianje 3"]
      },
      {
        id:"00000000000007", 
        firstName: "Maja",
        lastName: "Sanko",
        subjects: ["Diskretne strukture", "Programianje 2", "Multimedija"]
      }
    ];

    getStudents() {
        return new Promise((resolve, reject) => {
          // Simulate success/error.
          let error: any = false;
          // Get token.
          let clientToken = this.localStorageUtils.getToken();
          
          setTimeout(() => {
                  if (this.serverToken !== clientToken) {
                    reject("Token mismatch"); // Authentication error.
                  } else if (error) {
                      reject("error"); // Simulates error.
                  } else {
                      resolve(this.students); // Simulates success.
                  }
              }, 300);
          });
    }

    getStudent(id: any) {
      return new Promise((resolve, reject) => {
      // Simulate success/error.
      let error: any = false;
      // Get token.
      let clientToken = this.localStorageUtils.getToken();
      let student = this.students.find(i => i.id === id);

      setTimeout(() => {
              if (this.serverToken !== clientToken) {
                reject("Token mismatch"); // Authentication error.
              } else if (error) {
                  reject("error"); // Simulates error.
              } else if (student == null) {
                reject("error - student not found");
              } else {
                  resolve(student); // Simulates success.
              }
          }, 300);
      });
  }

   updateStudent(updatedStudent: any) {
    return new Promise((resolve, reject) => {
    // Simulate success/error.
    let error: any = false;
    let index: number = -1;
    // Get token.
    let clientToken = this.localStorageUtils.getToken();

    // Find index of the student inside students array.
    for ( let i = 0, len = this.students.length; i < len; i++) {
      if (this.students[i].id === updatedStudent.id) {
          index = i;
          break;
      }
    }
    
    setTimeout(() => {
            if (this.serverToken !== clientToken) {
              reject("Token mismatch"); // Authentication error.
            } else if (error || index < 0) {
             reject("error"); // Simulates error.
            } else {
              // Update student.
              this.students[index] = updatedStudent;
              resolve("success"); // Simulates success.
            }
        }, 300);
    });
  }

  createStudent(newStudent: any) {
    return new Promise((resolve, reject) => {
    // Simulate success/error.
    let error: any = false;
    // Get token.
    let clientToken = this.localStorageUtils.getToken();

    setTimeout(() => {
            if (this.serverToken !== clientToken) {
              reject("Token mismatch"); // Authentication error.
            } else if (error) {
                reject("error"); // Simulates error.
            } else {
                this.students.push(newStudent);
                resolve("success"); // Simulates success.
            }
        }, 300);
    });
}

 deleteStudent(index) {
      return new Promise((resolve, reject) => {
      // Simulate success/error.
      let error: any = false;
      // Get token.
      let clientToken = this.localStorageUtils.getToken();

      setTimeout(() => {
              if (this.serverToken !== clientToken) {
                reject("Token mismatch"); // Authentication error.
              } else if (error) {
                  reject("error"); // Simulates error.
              } else {
                  this.students.splice(index, 1);
                  resolve("success"); // Simulates success.
              }
          }, 300);
      });
  }
}