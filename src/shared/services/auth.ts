import {Injectable} from "@angular/core";

@Injectable()
export class AuthService {
  // Hardcoded subjects data. 
  serverToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dnZWRJbkFzIjoiYWRtaW4iLCJpYXQiOjE0MjI3Nzk2Mzh9.gzSraSYS8EXBxLN_oWnFSRgCzcmJmMjLiuyu5CSpyHI";
  credentials: any = {
    password: "1234567",
    email: "testni.email@mail.com"
  };

  signinUser(email, password) {
    return new Promise((resolve, reject) => {
    // Simulate success/error.
    let error: any = false;

    setTimeout(() => {
          if (error) {
              reject("error"); // Simulates error.
          } else if (email !== this.credentials.email || password !== this.credentials.password){
              reject("Invalid credentials");
          } else {
            resolve(this.serverToken); // Credentials match - return token.
          }
        }, 300);
    });
  }
}